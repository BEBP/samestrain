## Authors & Finding

<p>SimulPopB was developed by Audrey Duval, Sylvain Brisse and Lulla Opatowski. The original development of this tool was performed in the frame of the post-doc project of Audrey Duval supervized by Sylvain Brisse and Lulla Opatowski.</p>
<br>
<h3>People involved </h3>
<p>
Sylvain Brisse (Scientific manager, team leader, documentation, Initiator)<br>
Lulla Opatowski (Scientific manager, team leader, documentation)<br>
Audrey Duval (coding, testing, documentation, evaluation)
</p>
<br>
<h2>Funding</h2>
<p>This work was financially supported by the MedVetKlebs project, a component of European Joint Programme One Health EP, which has received funding from the European Union’s Horizon 2020 Research and Innovation Programme under Grant Agreement No. 773830.</p>
<br><br>







