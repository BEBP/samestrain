## FAQ

<h2>What is SimulPopB ? </h2>
<p> SimulPopB is a web tool that allow you to find the genetic cut-off that group outbreak cases and exclude outsiders. Two genetic features can be used: SNP or cgMLST data. This tool is composed of a Wright-Fisher forward model that simulate the evolution of a given bacteria. First, it start with a naive bacteria that will accumulate mutation according to the number of mutations per site per year during the duration of outbreak. At the end, SimulPopB will give you a pairwise genetic distance difference distribution as well as the genetic cut-off. You can also estimate the duration of your outbreak or the number of mutation per site per year with an MCMC method based on the modMCMC function of the <a href="https://cran.r-project.org/web/packages/FME/index.html">FME</a> R packages by <a href="https://www.jstatsoft.org/article/view/v033i03"> Stoetaert et al. </a></p><br>

<h2> What kind of data I need? </h2>
<p>To use SimulPop you need a sample dates file (.csv). First column is "ID", second is "dates" in "dd/mm/YYYY" or "YYYY-mm-dd". </p>

>|   ID      |    dates     |
>|:--------:|:-----------:|
>| 1            | 18/08/2020      |
>| 2            | 19/08/2020      |
>| 3            | 20/08/2020      |

<br>

<p>You also need parameters such as the duration of your outbreak, the number of mutations per site per year, the genome size (for SNP data) or the number of genes with their average size (for cgMLST data). Moreover, if you want to estimate the duration of outbreak or the number of mutations per site per year, you need a pairwise distance matrix of your genetic data (.csv) with first row as "ID".</p>

>|   1   |   2     |   3    |
>|:-----:|:-----:|:-----:|
>| 0   | 2     | 1    |
>| 2   | 0     | 2    |
>| 1   | 2     | 0    |

<br>


<h2> Simulation </h2>
This part allow you to find the genetic cut-off. First, you need to enter your type of genetic feature (SNP or cgMLST). Then, you enter the genome size for "SNP" or the average size of genes and the number of genes for "cgMLST". You can also decide the time step (day or month) for the simulation. If your duration of outbreak is high, month will give you quicker answer with less precision. The number of mutations per site per year is also required. Finally, you have to give your samples dates file in the "Choose CSV Date File" section. If you don't have a pairwise distance matrix, the tool will five you the simulated distribution of pairwise distance and the genetic cut-off in the "Distribution" section. If you have the pairwise distance matrix, the tool will group your samples according to the genetic cut-off found in the "Cluster" section. After giving you the genetic cut-off and the cluster, you will have the possibility to change the cut-off and see what append in the "Cluster" section.</p>
<br>
<h2>Estimation</h2>
<p>In the estimation part, you can estimate the duration of outbreak of the number of mutations per site per year. The pairwise distance matrix is required. As well as for the Simulation part, you need to enter your type of genetic feature (SNP or cgMLST). Then, you enter the genome size for "SNP" or the average size of genes and the number of genes for "cgMLST". You can also decide the time step (day or month) for the simulation and give your number of mutations per site per year for duration estimation or the duration of your outbreak for the mutation per site per year estimation. You can also choose the number of iteration for your MCMC, as well as the burnin and the number of chain you want. This last one is very important because the estimated parameter will be computed as the average estimation of the MCMC chain. Estimation needs minimal and maximal parameters that are the interval in which you think your parameters is. And a middle value.</p>    
<br>
<h2>Saving your findings </h2>
<p>All plots can be saved as pdf file with download button</p>










