<body>
<h2 style="color:MidnightBlue;font-weight: bold;font-size:20px">Getting started</h2>
<p>Enter the <b>Type</b> of cut-off you want. If you want a genetic cut-off based on SNP data enter the <b>Genome size</b> or if you want a genetic cut-off based on cgMLST data enter the <b>Average size of genes</b> and the <b>Number of genes</b>. Enter the duration of outbreak in <b>Duration</b> and the <b>Number of mutations</b> per site per year.</p> 
<p>You need a csv file with all swab dates to get the cut-off and/or a csv file with the pairwise distance between each isolates to discriminate outbreak to non outbreak cases according to the genetic cut-off found in the <b> CSV Files</b> box.</p>
</body>