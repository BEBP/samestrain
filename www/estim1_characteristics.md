<body>
<h2 style="color:DeepSkyBlue;font-weight: bold;font-size:20px">Getting started</h2>
<p>Enter the <b>Type</b> of cut-off you want. If you want a genetic cut-off based on SNP data enter the <b>Genome size</b> or if you want a genetic cut-off based on cgMLST data enter the <b>Average size of genes</b> and the <b>Number of genes</b>. Enter the <b>Number of mutations</b> per site per year and the duration unit in <b>Time step</b>.</p> 
<p>You need a csv file with all swab dates to get the cut-off and a csv file with the pairwise distance between each isolates to estimate the duration of outbreak with MCMC in the <b> CSV Files</b> box.</p>
<p>Enter limits of the estimated parameters in <b>Min</b> and <b>Max</b>. Enter a first start for the estimated parameters in <b>Middle</b>. If you want to complete the MCMC, you can add a <b>Number of iteration</b>, the <b>burning</b> and the <b>Number of chain</b> used for the estimated parameter (average of the chain).
</body>
