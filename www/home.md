## Welcome to Simulation Outbreak
<br>
<p style="font-size:24px">Find the cut-off of outbreak related cases and exclude outsiders with a Wright-Fisher forward model.
<br>
You can also find it in the SimulPopB R package.
<br>
Simulation Outbreak offer the option to estimate the duration or the number of mutations from a source with SNP (or allele from cgMLST data) differences distance matrix.</p>

<br>

<img style = 'display: block; margin-left: auto; margin-right: auto;' src='hom.png' width = '500'>

<br>
<p><b>Disclaimer </b></p>
<p>Please be aware that this is a proof-of-concept website made for academic research purpose only.
Do not use for medical diagnosis purposes.
This software comes with absolutely no warranty or dedicated support.</p>